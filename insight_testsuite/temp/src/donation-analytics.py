import heapq
import time, bisect
import pandas as pd
import pdb, sys
import datetime
import numpy as np

class RunPercentile(object):
    #A class to keep track of running percentile using 2 heaps
    #usage: rp = RunPercentile()
    #perc, tot, num = rp.push(val)
    def __init__(self, percentile):
        self.greater_min_heap = []
        self.lesser_max_heap = []
        self.perc_num = None
        self.percentile = percentile
        self.total = 0
    def push(self, num):
        self.total += num
        if self.percentile == 0 or self.percentile == 100: #deal with min or max separately
            if self.perc_num is None:
                self.perc_num = num
            else:
                diff = (self.perc_num - num) * (-1,1)[self.percentile==0]
                if diff > 0:
                    self.perc_num = num
        else:
            if self.perc_num is None or num < -self.lesser_max_heap[0]: #self.perc_num is None => first push
                heapq.heappush(self.lesser_max_heap,-num)
            else:
                heapq.heappush(self.greater_min_heap,num)

            self.rebalance()
            self.perc_num = -self.lesser_max_heap[0]
        return [self.perc_num, self.total, len(self.greater_min_heap) + len(self.lesser_max_heap)]

    def rebalance(self):
        tot = len(self.greater_min_heap) + len(self.lesser_max_heap)
        idx = int(np.ceil(tot*(self.percentile/100.0))) #note idx starts from 1 (and not 0)
        diff = len(self.lesser_max_heap) - idx
        assert diff in [0,1,-1]
        #We should always strive for diff==0
        if diff == 1:
            tmp = heapq.heappop(self.lesser_max_heap)
            heapq.heappush(self.greater_min_heap, -tmp)
        if diff == -1:
            tmp = heapq.heappop(self.greater_min_heap)
            heapq.heappush(self.lesser_max_heap, -tmp)

def round(x): #the inbuilt round function rounds 0.5 down. The specs mention, 0.5 is to be rounded up. hence redefining round() here
    diff = x-np.floor(x)
    return np.floor(x) if diff < 0.5 else np.floor(x)+1
            
def read_clean_ln(line):
    tmp = line.split('|')
    lninfo = {}
    for k, fieldname in zip([0,7,10,13,14,15], ['CMTE_ID', 'NAME', 'ZIP_CODE', 'TRANSACTION_DT', 'TRANSACTION_AMT', 'OTHER_ID']):
        lninfo[fieldname] = tmp[k]

    for k in lninfo:
        ln = len(lninfo[k])
        if k=='OTHER_ID':
            if ln!=0:
                return {}
        else:
            if ln==0:
                return {}
    if len(lninfo['ZIP_CODE'])<5:  #if zip code is too short, ignore this line
        return {}
    lninfo['ZIP_CODE'] = lninfo['ZIP_CODE'][:5]
    try:
        lninfo['TRANSACTION_AMT'] = int(round(float(lninfo['TRANSACTION_AMT'])))
    except:
        return {}
    
    #check if valid date
    if len(lninfo['TRANSACTION_DT'])!=8:
        return {}
    month = lninfo['TRANSACTION_DT'][:2]
    day = lninfo['TRANSACTION_DT'][2:4]
    year = lninfo['TRANSACTION_DT'][4:]
    try:  #try to form a date using datetime, to check if its valid
        datetime.datetime(year=int(year), month=int(month), day=int(day))
    except:
        return {}
    lninfo['TRANSACTION_DT'] = int(year)  #we only need the year
    return lninfo

    
def process(contfile, percfile, outfile):
    with open(percfile) as fperc:
	perc = int(fperc.readlines()[0].strip())
    with open(contfile) as fcont:
        contfilelines = fcont.readlines()
    donors = {}
    contribs = {}
    flg = False
    with open(outfile, 'w') as fout:
        for iii, line in enumerate(contfilelines):
            lninfo = read_clean_ln(line)
            if len(lninfo) == 0:  #skip invalid entry. for invalid entries read_clean_ln returns empty dictionary
                continue
            donor_id = (lninfo['NAME'], lninfo['ZIP_CODE'])
            donors[donor_id] = donors.get(donor_id, []) + [lninfo['TRANSACTION_DT']]
            
            #check if repeat donor
            repeatdonor = False
            for yr in donors[donor_id]:
                if yr < lninfo['TRANSACTION_DT']:
                    repeatdonor = True
                    break
            #if repeat donor, push to appropriate RunPercentile object
            if repeatdonor:
                contrib_id = (lninfo['CMTE_ID'], lninfo['ZIP_CODE'], lninfo['TRANSACTION_DT']) #perc, tot, numcontribs
                if contrib_id in contribs:
                    rp = contribs[contrib_id]
                else:
                    rp = RunPercentile(perc)
                    contribs[contrib_id] = rp
                percout = rp.push(lninfo['TRANSACTION_AMT']) #percout is a list of 3 things: current percentile number, current total, number of contributions
                newln = '\n' if flg else ''
                fout.write(newln + '|'.join([str(k) for k in list(contrib_id) + percout]))
                flg = True

if __name__ == '__main__':
    process(sys.argv[1], sys.argv[2], sys.argv[3])




