Running instructions:
Place the 2 input files in the input folder.
Run the .sh file like this: ./run.sh. An output file will be created in the output folder

For the test suite: cd insight_testsuite; ./run_tests.sh

In case of permission issues for either case, give it executable permission by: chmod +x ./run.sh or chmod +x ./run_tests.sh



Dependencies:
numpy
heapq
time
datetime



Approach:
The donation-analytics.py file contains all the required functions. Some of them are described below:

The RunPercentile class, has one function push. When push is called with a number, it returns the current percentile, the total of all the numbers and the number of numbers. This running percentile functionality is acheived by using 2 heaps (a min heap and a max heap), since the 2-heap implementation was found to be faster than "inserting into a sorted list" approach.

The process() function is called first, which reads the input perncentile file and the input data file. It goes through each line, cleans it using the read_clean_ln() function and rejects invalid records. Then we decide if the current donor is a "repeat donor" by checking in the donors dictionary, if that donor had contributed in an earlier year. If it is a repeat donor, we push the transaction amount into the RunPercentile object for that recepient/zip/year.



Comments:
Some contributions were negative. In this code I consider those as valid contribtions and do not treat them any differently.
